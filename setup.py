import os
import pkg_resources
from setuptools import setup, find_packages

def read_version(fname="sysMon/version.py"):
    exec(compile(open(fname, encoding="utf-8").read(), fname, "exec"))
    return locals()["__version__"]

setup(
    name='sysMon',
    version=read_version(),
    description='',
    author='Connor Etherington',
    author_email='connor@concise.cc',
    packages=find_packages(),
    install_requires=[
        str(r)
        for r in pkg_resources.parse_requirements(
            open(os.path.join(os.path.dirname(__file__), "requirements.txt"))
        )
    ],
    entry_points={'console_scripts': [
        'sysMon=sysMon.__main__:main'
    ]},
    include_package_data=True
)
