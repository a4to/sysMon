
import subprocess
from . import pr, verbose
import json
import time

def get_system_resources():
  # Get memory usage
  mem_output = subprocess.run(['free'], capture_output=True, text=True).stdout
  mem_lines = mem_output.split('\n')
  mem_parts = mem_lines[1].split()
  total_mem = int(mem_parts[1])
  used_mem = int(mem_parts[2])
  mem_percentage = (used_mem / total_mem) * 100

  # Get GPU usage
  gpu_output = subprocess.run(['nvidia-smi'], capture_output=True, text=True).stdout
  gpu_lines = gpu_output.split('\n')
  for line in gpu_lines:
    if 'MiB /' in line:
      gpu_mem_parts = line.split('|')[2].split()
      total_gpu_mem = int(gpu_mem_parts[2].replace('MiB', ''))
      used_gpu_mem = int(gpu_mem_parts[0].replace('MiB', ''))
      gpu_percentage = (used_gpu_mem / total_gpu_mem) * 100
      break

  return mem_percentage, gpu_percentage

def create_zbar(percentage, total_length=10):
  filled_length = int(total_length * percentage // 100)
  bar = 'x' * filled_length + '-' * (total_length - filled_length)
  return f"[{bar}] {percentage:.0f}%"

def main():
    with open('system_usage.txt', 'w') as f1, open('system_usage.json', 'w') as f2:
        while True:
            mem_percentage, gpu_percentage = get_system_resources()
            mem_bar = create_zbar(mem_percentage)
            gpu_bar = create_zbar(gpu_percentage)
            date = time.strftime("%Y-%m-%d %H:%M:%S")
            f1.write(f"| {date} | Memory: {mem_bar} | GPU: {gpu_bar}\n")
            if verbose:
                pr(f"| {date} | Memory: {mem_bar} | GPU: {gpu_bar}")
            f1.flush()
            data = {
            'date': date,
            'memory': mem_percentage,
            'gpu': gpu_percentage
            }
            json.dump(data, f2)
            f2.write('\n')
            f2.flush()
            time.sleep(0.5)


if __name__ == '__main__':
    main()
