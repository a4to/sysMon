import argparse
from .version import __version__
from colr import color

def pr(text: str, hex_color: str = None):
    blue_1 = '#67D0A8'
    if not hex_color:
        hex_color = blue_1
    print(color(text, fore=hex_color))


parser = argparse.ArgumentParser(description='')
parser.add_argument('-v', '--verbose', action='store_true', help='verbose output')

verbose = parser.parse_args().verbose
