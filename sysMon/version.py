import time
year, month, day, hour, min, sec, weekday, yearday, isdst  = time.gmtime()
__version__ = time.strftime("%Y%m%d", time.localtime())
